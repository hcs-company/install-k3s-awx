# Deploy AWX on K3S

This repository contains a basic set of Ansible roles to deploy (or upgrade) a
single-node K3S cluster, and to deploy Ansible AWX on that cluster.

## Playbooks

- `site.yaml` 
  An example playbook that runs all roles on a node called `k3s.testing`

- `credential_dump.yaml`
  An example playbook that run the `awx_credential_dump` role to display
  **decrypted** versions of all credentials stored in an AWX isntance

## Roles

- `roles/install_k3s`
  Installs or upgrades K3S as a single node cluster. This role assumes a RHEL9
  machine has been configured so that passwordless sudo works, firewalld is
  installed, and repos are available.

  This uses the setup script from [https://get.k3s.io](https://get.k3s.io)

- `roles/install_awx_operator`
  This role installs the AWX operator in the namespace `awx` on a single-node K3S cluster, using the kubeconfig from `/etc/rancher/k3s/k3s.yaml`.

  It install the version specified by the (default) variable `{{
  install_awx_operator_tag }}`, currently defaulting to `latest`.

  You can either use an actual release tag, like `2.19.0` in here, or set it to
  the magic string `latest` (the default) which will attempt to pull the latest
  release tag from the awx-operator github repo.

- `roles/deploy_awx`
  This role deploys or updates an AWX instance named `{{ deploy_awx_name }}`
  (default: `awx`) in the namespace `{{ deploy_awx_namespace }}` (default:
  `awx`) using Traefik ingress configured for `{{ deploy_awx_hostname }}`
  (default: `awx.testing`). It then displays the username and password for the
  default admin user.

- `roles/awx_credential_dump`
  This role attempts to decrypt all credentials stored in an AWX instance, and
  displays them.
